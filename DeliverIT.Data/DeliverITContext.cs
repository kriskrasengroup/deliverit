﻿using DeliverIT.Models.EntityModels;
using Microsoft.EntityFrameworkCore;

namespace DeliverIT.Data
{
    public class DeliverITContext : DbContext, IDeliverITContext
    {
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Parcel> Parcels { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Shipment> Shipments { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<ShipmentStatus> ShipmentStatuses { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }

        private readonly string _connectionString = "Server=.;Database=DeliverIT_DB;Trusted_Connection=True;MultipleActiveResultSets=true";

        public DeliverITContext(DbContextOptions options) : base(options)
        {
        }

        public DeliverITContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (!builder.IsConfigured)
            {
                builder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Shipment>()
                .HasOne(p => p.DestinationWarehouse)
                .WithMany(b => b.DestinationShipments)
                 .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Shipment>()
                .HasOne(p => p.OriginWarehouse)
                .WithMany(b => b.OriginShipments)
                 .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Address>()
               .HasOne(p => p.Country)
               .WithMany(b => b.Addresses)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Parcel>()
                .HasOne(p => p.Warehouse)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);

            base.OnModelCreating(builder);
        }
    }
}
