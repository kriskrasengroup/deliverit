﻿using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Services.Implementations
{
    public class AccountService : IAccountService
    {
        private readonly DeliverITContext _context;
        public AccountService(DeliverITContext context)

        {
            _context = context;
        }   

        public async Task<RegisterUserResponseModel> RegisterUser(RegisterUserRequestModel requestModel)
        {

            var userExists = await _context.Users.AnyAsync(p => p.Username == requestModel.Username);
            var customerExists = await _context.Customers.AnyAsync(p => p.Email == requestModel.Email);

            var result = new RegisterUserResponseModel();

            if (userExists || customerExists)
            {
                result.IsSuccess = false;
                result.Message = Constants.USER_CUSTOMER_ALREADY_EXISTS;
                return result;
            }

            var customerRoleId = await _context.Roles
                                               .Where(p => p.Name == "Customer")
                                               .Select(p => p.Id)
                                               .SingleAsync();
            var user = new User
            {
                Username = requestModel.Username,
                Password = requestModel.Password,
                RoleId = customerRoleId
            };

            _context.Users.Add(user);

            var countryIdResponse = await _context.Countries
                .Where(p => p.Name == requestModel.CountryName)
                .Select(p => new
                {
                    ContryId = p.Id
                })
                .SingleOrDefaultAsync();

            if (countryIdResponse == null)
            {
                result.IsSuccess = false;
                result.Message = Constants.COUNTRY_NOT_SUPPORTED;
                return result;
            }

            var cityIdResponse = await _context.Cities
               .Where(p => p.Name == requestModel.CityName)
               .Select(p => new
               {
                   CityId = p.Id
               })
               .SingleOrDefaultAsync();

            if (cityIdResponse == null)
            {
                result.IsSuccess = false;
                result.Message = Constants.CITY_NOT_SUPPORTED;
                return result;
            }

            var address = new Address
            {
                CityId = cityIdResponse.CityId,
                CountryId = countryIdResponse.ContryId,
                Street = requestModel.Address
            };

            await _context.Addresses.AddAsync(address);
            await _context.SaveChangesAsync();

            var userId = await _context.Users
                                             .Where(p => p.Username == requestModel.Username)
                                             .Select(p => p.Id)
                                             .FirstOrDefaultAsync();
            var customer = new Customer
            {
                Email = requestModel.Email,
                FirstName = requestModel.FirstName,
                LastName = requestModel.LastName,
                Address = address,
                UserId = userId
            };

            _context.Customers.Add(customer);

            await _context.SaveChangesAsync();

            result.IsSuccess = true;
            result.Message = Constants.CUSTOMER_REGISTERED;

            return result;
        }

        public async Task<GetLoggedUserInfoResponseModel> GetLoggedUserInfoAsync(GetLoggedUserInfoRequestModel requestModel)
        {
            var result = new GetLoggedUserInfoResponseModel();
            var userInfo = await (from u in _context.Users
                                  join r in _context.Roles on u.RoleId equals r.Id
                                  where u.Username.ToLower() == requestModel.Username.ToLower()
                                  select new GetUserInfoResponseModel
                                  {
                                      Username = u.Username,
                                      Password = u.Password,
                                      UserRoleName = r.Name,
                                      Id = u.Id
                                  }).SingleOrDefaultAsync();

            if (userInfo != null)
            {
                if (userInfo.Password != requestModel.Password)
                {
                    result.IsSuccess = false;
                    result.Message = Constants.PASSWORD_IS_INCORRECT;
                }
                else
                {
                    result.IsSuccess = true;
                    result.UserRoleName = userInfo.UserRoleName;
                    result.Id = userInfo.Id;
                    result.Message = Constants.USER_LOGGED;
                }
            }
            else
            {
                result.IsSuccess = false;
                result.Message = Constants.NO_SUCH_USER;
            }

            return result;
        }
    }
}
