﻿using DeliverIT.Models.ResponseModels;
using System;

namespace DeliverIT.Services.Helpers
{
    public static class PaginationHelper
    {
        public static PaginationResponse CalculatePages(int totalCount, int skip, int take)
        {
            var result = new PaginationResponse();
          
            if (totalCount < take)
            {
                result.CurrentPage = 1;
            }
            else
            {
                result.CurrentPage = skip / take;
                if (totalCount - skip > 0)
                {
                    result.CurrentPage++;
                }               
            }

            decimal pagesCalculation = totalCount / take;
            var numberOfPages = Math.Floor(pagesCalculation) + 1;

            if (numberOfPages == result.CurrentPage)
            {
                result.IsLastPage = true;
            }            

            return result;
        }
        
    }
}
