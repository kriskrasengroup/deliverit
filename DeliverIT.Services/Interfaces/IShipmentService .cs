﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using System.Threading.Tasks;

namespace DeliverIT.Services.Interfaces
{
    public interface IShipmentService
    {
        Task<CreateShipmentResponseModel> CreateShipmentAsync(CreateShipmentRequestModel requestModel);
        Task<UpdateShipmentResponseModel> UpdateShipmentAsync(UpdateShipmentRequestModel requestModel);
        Task<DeleteShipmentResponseModel> DeleteShipmentAsync(DeleteShipmentRequestModel requestModel);
        Task<GetShipmentsByWarehouseResponseModel> GetShipmentByOriginWarehouseAsync(GetShipmentsByWarehouseRequestModel requestModel);
        Task<GetShipmentsByWarehouseResponseModel> GetShipmentByDestinationWarehouseAsync(GetShipmentsByWarehouseRequestModel requestModel);
        Task<GetShipmentsByCustomerResponseModel> GetShipmentByCustomerAsync(GetShipmentsByCustomerRequestModel requestModel);
        Task<GetShipmentsResponse> GetShipmentAsync(GetPaginatedRequestModel requestModel);
        Task<GetShipmentResponseModel> GetShipmentByIdAsync(EditShipmentRequestModel requestModel);
    }
}
