﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DeliverIT.Services.Interfaces
{
    public interface IWarehouseService
    {
        Task<GetWarehouseResponseModel> GetWarehouseByCityAsync(GetWarehouseRequestModelByCity requestModel);
        Task<GetWarehouseResponseModel> GetWarehouseByCountryAsync(GetWarehouseRequestModelByCountry requestModel);
        Task<CreateWarehouseResponseModel> CreateWarehouseAsync(CreateWarehouseRequestModel requestModel);
        Task<UpdateWarehousesResponseModel> UpdateWarehouseAsync(UpdateWarehouseRequestModel requestModel);
        Task<DeleteWarehouseResponseModel> DeleteWarehouseAsync(DeleteWarehouseRequestModel requestModel);
        Task<TotalWarehousesResponseModel> TotalWarehousesAsync();
    }
}
