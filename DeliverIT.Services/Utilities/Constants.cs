﻿namespace DeliverIT.Services.Utilities
{
    public class Constants
    {
        public const string USER_CUSTOMER_ALREADY_EXISTS = "USER OR CUSTOMER ALREADY EXISTS";
        public const string COUNTRY_NOT_SUPPORTED = "COUNTRY NOT SUPPORTED";
        public const string CITY_NOT_SUPPORTED = "CITY NOT SUPPORTED";
        public const string CUSTOMER_REGISTERED = "YOU HAVE BEEN REGISTERED!";
        public const string PASSWORD_IS_INCORRECT = "PASSWORD IS INCORRECT";
        public const string USER_LOGGED = "YOU HAVE LOGGED";
        public const string NO_SUCH_USER = "NO SUCH USER!";
        public const string PARCEL_WRONG_PARAMETERS = "WRONG PARAMETERS";
        public const string PARCEL_CREATED = "PARCEL CREATED";
        public const string SHIPMENT_WRONG_PARAMETERS = "WRONG PARAMETERS";
        public const string SHIPMENT_CREATED = "SHIPMENT CREATED";
        public const string WAREHOUSE_ID_NOT_EXISTS = "WAREHOUSE ID YOU SELECTED DOES NOT EXIST";
        public const string NO_SUCH_SHIPMENT = "NO SUCH SHIPMENT";
        public const string SUCCESS_CREATED_WAREHOUSE = "SUCCESS CREATED WAREHOUSE";
        public const string ADDRESS_ID_NOT_EXISTS = "AddressID IS NOT EXIST";
        public const string WAREHOUSE_NOT_FOUND = "WAREHOUSE NOT FOUND";
        public const string WAREHOUSE_DELETED = "WAREHOUSE SUCCESSFULLY IS DELETED";
        public const string WAREHOUSE_CHANGED = "WAREHOUSE WAS CHANGED";
        public const string WAREHOUSE_FOUND = "WAREHOUSE WAS FOUND BUT THERE WAS NO CHANGES";
        public const string WAREHOUSE_WRONG_PARAMETERS = "WRONG PARAMETERS";
        public const string CUSTOMER_NOT_FOUND = "CUSTOMER NOT FOUND";
        public const string CUSTOMER_DELETED = "CUSTOMER SUCCESSFULLY IS DELETED";
        public const string CUSTOMER_CHANGED = "CUSTOMER WAS CHANGED";
        public const string CUSTOMER_FOUND = "CUSTOMER WAS FOUND BUT THERE WAS NO CHANGES";

        public const int PAGINATION_SIZE = 10;

        public const string EMAIL_REGEX = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$";

        public const string AdminRoleName = "Admin";
        public const string EmployeeRoleName = "Employee";
        public const string CustomerRoleName = "Customer";
    }
}
