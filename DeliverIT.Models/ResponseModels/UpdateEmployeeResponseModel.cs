﻿namespace DeliverIT.Models.ResponseModels
{
    public class UpdateEmployeeResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
