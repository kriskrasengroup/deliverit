﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Models.ResponseModels
{
    public class TotalWarehousesResponseModel
    {
        public long Warehouses { get; set; }
    }
}
