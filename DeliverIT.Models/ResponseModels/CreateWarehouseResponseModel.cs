﻿namespace DeliverIT.Models.ResponseModels
{
    public class CreateWarehouseResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

    }
}
