﻿using System.Collections.Generic;

namespace DeliverIT.Models.ResponseModels
{
    public class GetParcelsResponse : GetPaginatedResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public IEnumerable<GetParcelResponseModel> Data { get; set; }        
    }
}
