﻿using DeliverIT.Models.EntityModels;
using System.Collections.Generic;

namespace DeliverIT.Models.ResponseModels
{
    public class GetWarehouseResponseModel
    {
        public List<Warehouse> Warehouses { get; set; }
    }
}
