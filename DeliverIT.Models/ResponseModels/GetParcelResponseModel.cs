﻿using System;
using System.Collections.Generic;

namespace DeliverIT.Models.ResponseModels
{
    public class GetParcelResponseModel
    {
        public long Id { get; set; }
        public int Weight { get; set; }
        public bool DeliveryToAddress { get; set; }
        public long? CustomerId { get; set; }
        public int? WarehouseId { get; set; }
        public string CategoryName { get; set; }
        public long? ShipmentId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public IEnumerable<CategoryResponseModel> PossibleStatuses { get; set; }
    }
}
