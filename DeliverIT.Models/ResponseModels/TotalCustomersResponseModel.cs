﻿namespace DeliverIT.Models.ResponseModels
{
    public class TotalCustomersResponseModel
    {
        public long Customers { get; set; }
    }
}
