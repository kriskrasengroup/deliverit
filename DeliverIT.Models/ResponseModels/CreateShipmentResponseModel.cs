﻿namespace DeliverIT.Models.ResponseModels
{
    public class CreateShipmentResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
