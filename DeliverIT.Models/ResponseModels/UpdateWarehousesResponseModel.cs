﻿namespace DeliverIT.Models.ResponseModels
{
    public class UpdateWarehousesResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

    }
}
