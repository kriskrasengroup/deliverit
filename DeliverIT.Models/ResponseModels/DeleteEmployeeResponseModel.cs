﻿namespace DeliverIT.Models.ResponseModels
{
    public class DeleteEmployeeResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
