﻿using System.Collections.Generic;

namespace DeliverIT.Models.ResponseModels
{
    public class GetCountryResponseModel
    {
        public List<string> Countries { get; set; }
    }
}
