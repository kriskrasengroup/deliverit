﻿namespace DeliverIT.Models.ResponseModels
{
    public class DeleteParcelResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
