﻿namespace DeliverIT.Models.RequestModels
{
    public class UpdateEmployeeRequestModel
    {        
        public long EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public long? AddressId { get; set; }
    }
}