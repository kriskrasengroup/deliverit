﻿namespace DeliverIT.Models.RequestModels
{
    public class GetLoggedUserInfoRequestModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}