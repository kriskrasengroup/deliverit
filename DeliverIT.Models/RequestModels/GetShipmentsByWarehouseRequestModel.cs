﻿namespace DeliverIT.Models.RequestModels
{
    public class GetShipmentsByWarehouseRequestModel : GetPaginatedRequestModel
    {
        public int WarehouseId { get; set; }
    }
}