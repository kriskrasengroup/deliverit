﻿using System;

namespace DeliverIT.Models.RequestModels
{
    public class UpdateShipmentRequestModel
    {
        public long Id { get; set; }
        public int OriginWarehouseId { get; set; }
        public int DestinationWarehouseId { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public byte StatusId { get; set; }
    }
}