﻿namespace DeliverIT.Models.RequestModels
{
    public class GetParcelsRequestModel : GetPaginatedRequestModel
    {
        public string UserRole { get; set; }
        public long UserId { get; set; }
    }
}