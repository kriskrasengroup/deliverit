﻿namespace DeliverIT.Models.RequestModels
{
    public class GetWarehouseRequestModelByCity
    {
        public string CityName { get; set; }
    }
}