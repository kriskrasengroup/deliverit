﻿namespace DeliverIT.Models.RequestModels
{
    public class DeleteCustomerRequestModel
    {
        public long Id { get; set; }
    }
}