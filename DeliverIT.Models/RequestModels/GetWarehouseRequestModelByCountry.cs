﻿namespace DeliverIT.Models.RequestModels
{
    public class GetWarehouseRequestModelByCountry
    {
        public string CountryName { get; set; }
    }
}