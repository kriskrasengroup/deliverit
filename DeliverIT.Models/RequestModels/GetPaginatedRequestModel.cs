﻿namespace DeliverIT.Models.RequestModels
{
    public class GetPaginatedRequestModel
    {
        private const int DEFAULT_PAGINATION_SIZE = 10;
        public int Skip { get; set; }
        public int Take { get; set; } = DEFAULT_PAGINATION_SIZE;
    }
}