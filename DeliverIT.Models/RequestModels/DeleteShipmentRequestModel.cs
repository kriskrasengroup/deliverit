﻿namespace DeliverIT.Models.RequestModels
{
    public class DeleteShipmentRequestModel
    {
        public long Id { get; set; }
    }
}