﻿const gridOptions = {
    columnDefs: columnDefs,
    rowData: []
};

const eGridDiv = document.querySelector('#myGrid');
new agGrid.Grid(eGridDiv, gridOptions);
gridOptions.api.sizeColumnsToFit();

getData(currentPageNumber);

function drawPagination(totalElements, currentPage) {

    let numberOfPages = Math.floor(totalElements / PageSize) + 1;

    let $prevButton = $("#prev-button");
    let $nextButton = $("#next-button")

    if (currentPage == 1) {
        $nextButton.prop("disabled", true);
        if (numberOfPages > 1) {
            $prevButton.prop("disabled", false);
        }
    }
    else if (currentPage == numberOfPages) {
        $prevButton.prop("disabled", true);
        if (numberOfPages > 1) {
            $nextButton.prop("disabled", false);
        }
    }   
    else if (currentPage < numberOfPages) {
        $prevButton.prop("disabled", false);
        $nextButton.prop("disabled", false);
    }

    $("#page-counter").text("Page: " + currentPage);
}

$("#myGrid").on("click", ".delete-parcel-button", function (event) {
    let parcelId = parseInt((($(event)[0]).currentTarget).getAttribute("data-id"));

    let dataToSend = JSON.stringify({
        id: parcelId
    });

    $.ajax({
        url: "/api/Parcel/Delete",
        type: "DELETE",
        contentType: "application/json; charset=utf-8",
        data: dataToSend,
        success: function (response) {

            getData(currentPageNumber);
            
        },
        error: function () {
            
        }
    });
});

$("#prev-button").click(function (event) {
    getPreviousPageData();
});

$("#next-button").click(function (event) {
    getNextPageData();
});

function getPreviousPageData() {
    if (currentPageNumber > 0) {
        currentPageNumber--;
        getData(currentPageNumber);
    }
}

function getNextPageData() {
    currentPageNumber++;
    getData(currentPageNumber);
}

function getData(newPageNumber) {
    let skip = (newPageNumber * PageSize) - PageSize;

    let dataToSend = JSON.stringify({
        take: PageSize,
        skip: skip
    });

    $.ajax({
        url: "/api/Parcel/GetParcels",
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            gridOptions.api.setRowData(response.data);
            drawPagination(response.totalCount, response.currentPage)
        },
        error: function (response) {
          
        }
    });
}
