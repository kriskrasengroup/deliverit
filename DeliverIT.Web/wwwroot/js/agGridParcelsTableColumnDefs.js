﻿let currentPageNumber = 1;
const PageSize = 10;

var columnDefs;

function setColumnDefs(userRole) {
    columnDefs = [
        { headerName: 'Id', field: 'id', resizable: true },
        { headerName: 'Weight', field: 'weight', resizable: true },
        { headerName: 'Delivery To Address', field: 'deliveryToAddress', resizable: true },
        { headerName: 'Customer Id', field: 'customerId', resizable: true },
        { headerName: 'Warehouse Id', field: 'warehouseId', resizable: true },
        { headerName: 'Category Name', field: 'categoryName', resizable: true },
        { headerName: 'Shipment Id', field: 'shipmentId', resizable: true },
        { headerName: 'Arrival Date', field: 'arrivalDate', resizable: true }];

        if (userRole == "customer") {
            //columnDefs.push({
            //    headerName: "Edit Action",
            //    field: "button",
            //    cellRenderer: function (params) {
            //        return `<a type="button" class="edit-parcel-button custom-button-link btn btn-primary" href="/api/Parcel/EditParcel?id=${params.data.id}">Edit</a>`;
            //    }
            //});         
        }
        else {

            columnDefs.push({
                headerName: "Edit Action",
                    field: "button",
                        cellRenderer: function (params) {
                            return `<a type="button" class="edit-parcel-button custom-button-link btn btn-primary" href="/api/Parcel/EditParcel?id=${params.data.id}">Edit</a>`;
                        }
            },
            {
                headerName: "Delete Action",
                    field: "button",
                        cellRenderer: function (params) {
                            return `<button class="delete-parcel-button btn btn-danger" row-index=${params.rowIndex} data-id=${params.data.id} href="#">Delete</button>`;
                        }
                });
            
        }
}