﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    [Route("/api/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService _employeeServices;

        public EmployeeController(IEmployeeService employeeServices)
        {
            _employeeServices = employeeServices;
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterEmployeeRequestModel requestModel)
        {
            var result = await _employeeServices.RegisterEmployeeAsync(requestModel);


            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] DeleteEmployeeRequestModel requestModel)
        {
            var result = await _employeeServices.DeleteEmployeeAsync(requestModel);

            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateEmployeeRequestModel requestModel)
        {
            var result = await _employeeServices.UpdateEmployeeAsync(requestModel);

            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }
    }
}
