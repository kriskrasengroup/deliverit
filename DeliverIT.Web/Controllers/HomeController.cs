﻿using DeliverIT.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAccountService _accountServices;

        public HomeController(IAccountService accountServices)
        {
            _accountServices = accountServices;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {            
            return View();
        }
        public async Task<IActionResult> About()
        {

            return View();
        }
        public async Task<IActionResult> Contact()
        {
            return View();
        }
    }
}
