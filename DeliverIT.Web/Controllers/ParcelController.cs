﻿using DeliverIT.Models.RequestModels;
using DeliverIT.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ParcelController : Controller
    {
        private readonly IParcelService _parcelServices;

        public ParcelController(IParcelService parcelServices)
        {
            _parcelServices = parcelServices;
        }

        [HttpPost]
        [Authorize(Roles = "Employee, Admin")]
        public async Task<IActionResult> Create([FromBody] CreateParcelRequestModel requestModel)
        {
            var response = await _parcelServices.CreateParcelAsync(requestModel);
            return Json(response);
        }

        [HttpPut]
        [Authorize(Roles = "Employee, Admin")]
        public async Task<IActionResult> Update([FromQuery] UpdateParcelRequestModel requestModel)
        {
            var response = await _parcelServices.UpdateParcelAsync(requestModel);
            return Json(response);
        }

        [HttpDelete]
        [Authorize(Roles = "Employee, Admin")]
        public async Task<IActionResult> Delete(DeleteParcelRequestModel requestModel)
        {
            var result = await _parcelServices.DeleteParcelAsync(requestModel);
            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return BadRequest(result.Message);
        }

        [HttpGet]
        [Authorize(Roles = "Employee, Admin")]
        public async Task<IActionResult> Get([FromQuery] FilterParcelRequesteModel requestModel)
        {
            var response = await _parcelServices.FilterParcelAsync(requestModel);
            return Json(response);
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> CreateParcel()
        {
            return View(new CreateParcelRequestModel());
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> ShowParcels()
        {
            return View();
        }

        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetParcels(GetPaginatedRequestModel requestModel)
        {
            var request = new GetParcelsRequestModel
            {
                Skip = requestModel.Skip,
                Take = requestModel.Take,
                UserRole = User.Claims.FirstOrDefault(p => p.Type == ClaimTypes.Role)?.Value,
                UserId = long.Parse(User.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier)?.Value)
            };

            var result = await _parcelServices.GetParcelAsync(request);
            if (result.IsSuccess)
            {
                return Json(result);
            }
            return BadRequest(result.Message);
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> EditParcel([FromQuery] EditParcelRequestModel requestModel)
        {
            var result = await _parcelServices.GetParcelByIdAsync(requestModel);
            return View(result);
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> ShowCustomerParcels()
        {
            return View("ShowCustomerParcels");
        }

        [HttpPost]
        public async Task<IActionResult> UpdateParcel([FromForm] UpdateParcelRequestModel requestModel)
        {
            var result = await _parcelServices.UpdateParcelAsync(requestModel);
            if (result.IsSuccess)
            {
                return RedirectToAction("ShowParcels");
            }
            return BadRequest(result.Message);
        }

        [HttpGet]
        public async Task<IActionResult> ShowParcelMvc()
        {
            return View();
        }
    }
}
