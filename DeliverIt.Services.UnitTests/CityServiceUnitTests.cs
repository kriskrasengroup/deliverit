﻿using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Services.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Services.UnitTests
{
    [TestClass]
    public class CityServiceUnitTests
    {
        [TestMethod]
        public async Task CityService_ShouldShow_AllCitiesCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCities = new List<City>()
            {
                new City
                {
                    Id = 1,
                    Name = "Lom"
                },
                new City
                {
                    Id = 2,
                    Name = "Sofia"
                },
                new City
                {
                    Id = 3,
                    Name = "Varna"
                }
            };

            var mockDbSet = mockCities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Cities).Returns(mockDbSet.Object);

            var service = new CityService(mockContext.Object);

            var result = await service.GetCities();
            Assert.AreEqual(result.Cities.Count, mockCities.Count);
        }
    }
}
