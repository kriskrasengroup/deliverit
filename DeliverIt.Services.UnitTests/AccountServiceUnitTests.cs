using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Services.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIt.Services.UnitTests
{
    [TestClass]
    public class AccountServiceUnitTests
    {
        [TestMethod]
        public async Task AccountService_ShouldCreate_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role 
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>();
            var users = new List<User>();
            var customers = new List<Customer>();
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new AccountService(mockContext.Object);

            var request = new RegisterUserRequestModel()
            {
                Username = "TestUser",
                Password = "12345",
                FirstName = "Test",
                LastName = "User",
                Email = "testemail@testMail.com",
                CountryName = "Bulgaria",
                CityName = "Burgas",
                Address = "123 test str."
            };
            var result = await service.RegisterUser(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task AccountService_ShouldReturnFalse_WhenUserExists()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>();
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var customers = new List<Customer>();
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new AccountService(mockContext.Object);

            var request = new RegisterUserRequestModel()
            {
                Username = "TestUser",
                Password = "12345",
                FirstName = "Test",
                LastName = "User",
                Email = "testemail@testMail.com",
                CountryName = "Bulgaria",
                CityName = "Burgas",
                Address = "123 test str."
            };
            var result = await service.RegisterUser(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task AccountService_ShouldReturnFalse_WhenCountryNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>();
            var users = new List<User>();
            var customers = new List<Customer>();
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "WrongCountry"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new AccountService(mockContext.Object);

            var request = new RegisterUserRequestModel()
            {
                Username = "TestUser",
                Password = "12345",
                FirstName = "Test",
                LastName = "User",
                Email = "testemail@testMail.com",
                CountryName = "Bulgaria",
                CityName = "Burgas",
                Address = "123 test str."
            };
            var result = await service.RegisterUser(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task AccountService_ShouldReturnFalse_WhenCityNotCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>();
            var users = new List<User>();
            var customers = new List<Customer>();
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "WrongCity"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new AccountService(mockContext.Object);

            var request = new RegisterUserRequestModel()
            {
                Username = "TestUser",
                Password = "12345",
                FirstName = "Test",
                LastName = "User",
                Email = "testemail@testMail.com",
                CountryName = "Bulgaria",
                CityName = "Burgas",
                Address = "123 test str."
            };
            var result = await service.RegisterUser(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task AccountService_LoginShouldReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>();
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var customers = new List<Customer>();
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new AccountService(mockContext.Object);

            var request = new GetLoggedUserInfoRequestModel()
            {
                Username = "TestUser",
                Password = "12345"               
            };
            var result = await service.GetLoggedUserInfoAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task AccountService_LoginShouldReturnFalse_WhenUserNotFound()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>();
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser2",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var customers = new List<Customer>();
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new AccountService(mockContext.Object);

            var request = new GetLoggedUserInfoRequestModel()
            {
                Username = "TestUser",
                Password = "12345"
            };
            var result = await service.GetLoggedUserInfoAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task AccountService_LoginShouldReturnFalse_WhenPasswordWrong()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>();
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "54321",
                    RoleId = 1
                }
            };
            var customers = new List<Customer>();
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new AccountService(mockContext.Object);

            var request = new GetLoggedUserInfoRequestModel()
            {
                Username = "TestUser",
                Password = "12345"
            };
            var result = await service.GetLoggedUserInfoAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }
    }
}