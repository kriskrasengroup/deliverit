﻿using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Services.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Services.UnitTests
{
    [TestClass]
    public class CountryServiceUnitTests
    {
        [TestMethod]
        public async Task CountryService_ShouldDisplay_AllCountriesCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria"
                },
                new Country
                {
                    Id = 2,
                    Name = "Greece"
                },
                new Country
                {
                    Id = 3,
                    Name = "Austria"
                }
            };

            var mockDbSet = mockCountries.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Countries).Returns(mockDbSet.Object);

            var service = new CountryService(mockContext.Object);

            var result = await service.GetCountries();
            Assert.AreEqual(result.Countries.Count, mockCountries.Count);
        }
    }
}
