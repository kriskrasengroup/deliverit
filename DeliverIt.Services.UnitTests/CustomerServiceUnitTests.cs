using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Services.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIt.Services.UnitTests
{
    [TestClass]
    public class CustomerServiceUnitTests
    {
        [TestMethod]
        public async Task CustomerService_ShouldDelete_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role 
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1                    
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    AddressId = 1,
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new CustomerService(mockContext.Object);

            var request = new DeleteCustomerRequestModel()
            {
               Id = 1
            };
            var result = await service.DeleteCustomerAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task CustomerService_DeleteReturnFalse_WhenUserNotFound()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    AddressId = 1,
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new CustomerService(mockContext.Object);

            var request = new DeleteCustomerRequestModel()
            {
                Id = 2
            };
            var result = await service.DeleteCustomerAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task CustomerService_UpdateReturnTrue_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                },
                new Address
                {
                    Id = 2,
                    Street = "Hristo Botev",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    AddressId = 1,
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new CustomerService(mockContext.Object);

            var request = new UpdateCustomerRequestModel()
            {
                CustomerId = 1,
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "TestMail@TestMail.com",
                AddressId = 2
            };
            var result = await service.UpdateCustomerAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public async Task CustomerService_UpdateReturnFalse_WhenNoChanges()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                },
                new Address
                {
                    Id = 2,
                    Street = "Hristo Botev",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    AddressId = 1,
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new CustomerService(mockContext.Object);

            var request = new UpdateCustomerRequestModel()
            {
                CustomerId = 1,
                FirstName = "Test",
                LastName = "User",
                Email = "testmail@mailtest.com",
                AddressId = 1
            };
            var result = await service.UpdateCustomerAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task CustomerService_UpdateReturnFalse_WhenUserNotFound()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                },
                new Address
                {
                    Id = 2,
                    Street = "Hristo Botev",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    AddressId = 1,
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new CustomerService(mockContext.Object);

            var request = new UpdateCustomerRequestModel()
            {
                CustomerId = 2,
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "TestMail@TestMail.com",
                AddressId = 2
            };
            var result = await service.UpdateCustomerAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
        }

        [TestMethod]
        public async Task TotalCustomersAsync_ShouldReturnCount()
        {
            var mockContext = new Mock<DeliverITContext>();

            var roles = new List<Role>
            {
                new Role
                {
                    Id = 1,
                    Name = "Customer"
                },
                new Role
                {
                    Id = 2,
                    Name = "Employee"
                },
                new Role
                {
                    Id = 3,
                    Name = "Admin"
                }
            };

            var address = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1
                },
                new Address
                {
                    Id = 2,
                    Street = "Hristo Botev",
                    CityId = 1
                }
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Username = "TestUser",
                    Password = "12345",
                    RoleId = 1
                }
            };
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "testmail@mailtest.com",
                    AddressId = 1,
                    UserId = 1
                }
            };
            var countries = new List<Country>
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };
            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Burgas"
                }
            };

            var mockUsers = users.AsQueryable().BuildMockDbSet();
            var mockCustomers = customers.AsQueryable().BuildMockDbSet();
            var mockRoles = roles.AsQueryable().BuildMockDbSet();
            var mockAddresses = address.AsQueryable().BuildMockDbSet();
            var mockCountries = countries.AsQueryable().BuildMockDbSet();
            var mockCities = cities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Users).Returns(mockUsers.Object);
            mockContext.Setup(db => db.Customers).Returns(mockCustomers.Object);
            mockContext.Setup(db => db.Roles).Returns(mockRoles.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddresses.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountries.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCities.Object);


            var service = new CustomerService(mockContext.Object);

            var request = new UpdateCustomerRequestModel()
            {
                CustomerId = 2,
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "TestMail@TestMail.com",
                AddressId = 2
            };
            var result = await service.TotalCustomersAsync();
            Assert.AreEqual(result.Customers, 1);
        }
    }
}