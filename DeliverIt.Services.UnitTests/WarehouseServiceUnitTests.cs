﻿using DeliverIT.Data;
using DeliverIT.Models.EntityModels;
using DeliverIT.Models.RequestModels;
using DeliverIT.Models.ResponseModels;
using DeliverIT.Services.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DeliverIT.Services.UnitTests
{
    [TestClass]
    public class WarehouseServiceUnitTests
    {
        [TestMethod]
        public async Task WarehouseService_ShouldGet_ReturnCorrectAddress()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId = 1
                }
            };

            var mockAddresses = new List<Address>()
            {
                new Address()
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1,
                    City = mockCities[0]
                }
            };
            var mockWarehouses = new List<Warehouse>()
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1,
                     Address = mockAddresses[0]
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2,
                     Address = mockAddresses[0]
                }
            };
            var mockWarehousesDbSet = mockWarehouses.AsQueryable().BuildMockDbSet();
            var mockCountriesDbSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesDbSet = mockCities.AsQueryable().BuildMockDbSet();
            var mockAddressesDbSet = mockAddresses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Warehouses).Returns(mockWarehousesDbSet.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountriesDbSet.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCitiesDbSet.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddressesDbSet.Object);

            var service = new WarehouseService(mockContext.Object);

            var request = new GetWarehouseRequestModelByCity()
            {
                CityName = "Sofia"
            };

            var result = await service.GetWarehouseByCityAsync(request);
            Assert.AreEqual(result.Warehouses.Count, mockWarehouses.Count);
        }

        [TestMethod]
        public async Task WarehouseService_ShouldThrowExceptionWhenGivenIdDoesntExist()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId = 1,
                    Country = mockCountries[0]
                }
            };

            var mockAddresses = new List<Address>()
            {
                new Address()
                {
                    Id = 1,
                    Street = "Street",
                    CityId = 1,
                    City = mockCities[0]
                }
            };

            var mockWarehouses = new List<Warehouse>()
            {
                new Warehouse()
                {
                    Id = 1,
                    AddressId = 1,
                    Address = mockAddresses[0]
            }  };

            var mockCountriesDbSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesDbSet = mockCities.AsQueryable().BuildMockDbSet();
            var mockAddressesDbSet = mockAddresses.AsQueryable().BuildMockDbSet();
            var mockWarehousesDbSet = mockWarehouses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Warehouses).Returns(mockWarehousesDbSet.Object);
            mockContext.Setup(db => db.Countries).Returns(mockCountriesDbSet.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCitiesDbSet.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddressesDbSet.Object);

            var service = new WarehouseService(mockContext.Object);

            var request = new GetWarehouseRequestModelByCountry()
            {
                CountryName = "Bulgaria"
            };
            var result = await service.GetWarehouseByCountryAsync(request);

            Assert.AreEqual(result.Warehouses.Count, mockWarehouses.Count);

        }

        [TestMethod]
        public async Task WarehouseService_ShouldCreate_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId = 1,
                    Country = mockCountries[0]
                },
                new City()
                {
                    Id = 2,
                    Name = "Varna",
                    CountryId = 1,
                    Country = mockCountries[0]
            }  };

            var mockAddresses = new List<Address>()
            {
                new Address()
                {
                    Id = 1,
                    Street = "Street",
                    CityId = 1,
                    City = mockCities[0]
                },

                new Address()
                {
                    Id = 2,
                    Street = "Street1",
                    CityId = 1,
                    City = mockCities[1]
                }
            };

            var warehouses = new List<Warehouse>();

            var mockWarehouses = new List<Warehouse>()
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1,
                     Address = mockAddresses[0]
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2,
                     Address = mockAddresses[1]
                }
            };
            var mockCountriesDbSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesDbSet = mockCities.AsQueryable().BuildMockDbSet();
            var mockAddressesDbSet = mockAddresses.AsQueryable().BuildMockDbSet();
            var mockkWarehousesDbSet = warehouses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Countries).Returns(mockCountriesDbSet.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCitiesDbSet.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddressesDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockkWarehousesDbSet.Object);
            mockContext.Setup(db => db.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();

            var service = new WarehouseService(mockContext.Object);

            var request = new CreateWarehouseRequestModel()
            {
                AddressId = 1
            };

            var result = await service.CreateWarehouseAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(CreateWarehouseResponseModel));

            mockContext.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public async Task WarehouseService_ShouldCreate_WhenParameters_NotValid()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId = 1,
                    Country = mockCountries[0]
                },
                new City()
                {
                    Id = 2,
                    Name = "Varna",
                    CountryId = 1,
                    Country = mockCountries[0]
            }  };

            var mockAddresses = new List<Address>()
            {
                new Address()
                {
                    Id = 1,
                    Street = "Street",
                    CityId = 1,
                    City = mockCities[0]
                },

                new Address()
                {
                    Id = 2,
                    Street = "Street1",
                    CityId = 1,
                    City = mockCities[1]
                }
            };

            var warehouses = new List<Warehouse>();

            var mockWarehouses = new List<Warehouse>()
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1,
                     Address = mockAddresses[0]
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2,
                     Address = mockAddresses[1]
                }
            };
            var mockCountriesDbSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesDbSet = mockCities.AsQueryable().BuildMockDbSet();
            var mockAddressesDbSet = mockAddresses.AsQueryable().BuildMockDbSet();
            var mockkWarehousesDbSet = warehouses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Countries).Returns(mockCountriesDbSet.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCitiesDbSet.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddressesDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockkWarehousesDbSet.Object);

            var service = new WarehouseService(mockContext.Object);

            var request = new CreateWarehouseRequestModel()
            {
                AddressId = -1
            };

            var result = await service.CreateWarehouseAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(CreateWarehouseResponseModel));
        }

        [TestMethod]
        public async Task WarehouseService_ShouldUpdate_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId = 1
                },
                new City()
                {
                    Id = 2,
                    Name = "Varna",
                    CountryId = 1
            }  };

            var mockAddresses = new List<Address>()
            {
                new Address()
                {
                    Id = 1,
                    Street = "Street",
                    CityId = 1,
                    CountryId = 1
                },

                new Address()
                {
                    Id = 2,
                    Street = "Street1",
                    CityId = 1,
                    CountryId = 1
                }
            };

            var mockWarehouses = new List<Warehouse>()
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockCountriesDbSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesDbSet = mockCities.AsQueryable().BuildMockDbSet();
            var mockAddressesDbSet = mockAddresses.AsQueryable().BuildMockDbSet();
            var mockWarehousesDbSet = mockWarehouses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Countries).Returns(mockCountriesDbSet.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCitiesDbSet.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddressesDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockWarehousesDbSet.Object);

            mockContext.Setup(db => db.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();

            var service = new WarehouseService(mockContext.Object);

            var request = new UpdateWarehouseRequestModel()
            {
                Id = 1,
                AddressId = 2
            };

            var result = await service.UpdateWarehouseAsync(request);

            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task WarehouseService_UpdateShouldReturnFalse_When_IdSame()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId = 1
                },
                new City()
                {
                    Id = 2,
                    Name = "Varna",
                    CountryId = 1
            }  };

            var mockAddresses = new List<Address>()
            {
                new Address()
                {
                    Id = 1,
                    Street = "Street",
                    CityId = 1,
                    CountryId = 1
                },

                new Address()
                {
                    Id = 2,
                    Street = "Street1",
                    CityId = 1,
                    CountryId = 1
                }
            };

            var mockWarehouses = new List<Warehouse>()
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockCountriesDbSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesDbSet = mockCities.AsQueryable().BuildMockDbSet();
            var mockAddressesDbSet = mockAddresses.AsQueryable().BuildMockDbSet();
            var mockWarehousesDbSet = mockWarehouses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Countries).Returns(mockCountriesDbSet.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCitiesDbSet.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddressesDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockWarehousesDbSet.Object);

            mockContext.Setup(db => db.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();

            var service = new WarehouseService(mockContext.Object);

            var request = new UpdateWarehouseRequestModel()
            {
                Id = 1,
                AddressId = 1
            };

            var result = await service.UpdateWarehouseAsync(request);

            Assert.AreEqual(result.IsSuccess, false);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task WarehouseService_ShouldUpdateReturnsFalse_WhenParameters_NotValid()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId = 1,
                    Country = mockCountries[0]
                },
                new City()
                {
                    Id = 2,
                    Name = "Varna",
                    CountryId = 1,
                    Country = mockCountries[0]
            }  };

            var mockAddresses = new List<Address>()
            {
                new Address()
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1,
                    City = mockCities[0]
                },

                new Address()
                {
                    Id = 2,
                    Street = "GeorgiRaychev",
                    CityId = 1,
                    City = mockCities[1]
                }
            };

            var mockWarehouses = new List<Warehouse>()
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1,
                     Address = mockAddresses[0]
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2,
                     Address = mockAddresses[1]
                }
            };
            var mockCountriesDbSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesDbSet = mockCities.AsQueryable().BuildMockDbSet();
            var mockAddressesDbSet = mockAddresses.AsQueryable().BuildMockDbSet();
            var mockWarehousesDbSet = mockWarehouses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Countries).Returns(mockCountriesDbSet.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCitiesDbSet.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddressesDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockWarehousesDbSet.Object);

            var service = new WarehouseService(mockContext.Object);

            var request = new UpdateWarehouseRequestModel()
            {
                Id = -1,
                AddressId = -1
            };

            var result = await service.UpdateWarehouseAsync(request);

            Assert.AreEqual(result.IsSuccess, false);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task WarehouseService_ShouldDelete_WhenParametersCorrect()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId = 1
                },
                new City()
                {
                    Id = 2,
                    Name = "Varna",
                    CountryId = 1
            }  };

            var mockAddresses = new List<Address>()
            {
                new Address()
                {
                    Id = 1,
                    Street = "Street",
                    CityId = 1,
                    CountryId = 1
                },

                new Address()
                {
                    Id = 2,
                    Street = "Street1",
                    CityId = 1,
                    CountryId = 1
                }
            };

            var mockWarehouses = new List<Warehouse>()
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockCountriesDbSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesDbSet = mockCities.AsQueryable().BuildMockDbSet();
            var mockAddressesDbSet = mockAddresses.AsQueryable().BuildMockDbSet();
            var mockWarehousesDbSet = mockWarehouses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Countries).Returns(mockCountriesDbSet.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCitiesDbSet.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddressesDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockWarehousesDbSet.Object);

            mockContext.Setup(db => db.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();

            var service = new WarehouseService(mockContext.Object);

            var request = new DeleteWarehouseRequestModel()
            {
                Id = 1
            };

            var result = await service.DeleteWarehouseAsync(request);

            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task WarehouseService_ShouldDelete_WhenParameters_NotValid()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId = 1,
                    Country = mockCountries[0]
                },
                new City()
                {
                    Id = 2,
                    Name = "Varna",
                    CountryId = 1,
                    Country = mockCountries[0]
            }  };

            var mockAddresses = new List<Address>()
            {
                new Address()
                {
                    Id = 1,
                    Street = "Lozengrad",
                    CityId = 1,
                    City = mockCities[0]
                },

                new Address()
                {
                    Id = 2,
                    Street = "GeorgiRaychev",
                    CityId = 1,
                    City = mockCities[1]
                }
            };

            var mockWarehouses = new List<Warehouse>()
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1,
                     Address = mockAddresses[0]
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2,
                     Address = mockAddresses[1]
                }
            };
            var mockCountriesDbSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesDbSet = mockCities.AsQueryable().BuildMockDbSet();
            var mockAddressesDbSet = mockAddresses.AsQueryable().BuildMockDbSet();
            var mockWarehousesDbSet = mockWarehouses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Countries).Returns(mockCountriesDbSet.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCitiesDbSet.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddressesDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockWarehousesDbSet.Object);

            var service = new WarehouseService(mockContext.Object);

            var request = new DeleteWarehouseRequestModel()
            {
                Id = -1
            };

            var result = await service.DeleteWarehouseAsync(request);

            Assert.AreEqual(result.IsSuccess, false);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task WarehouseService_ReturnCount()
        {
            var mockContext = new Mock<DeliverITContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    Id = 1,
                    Name = "Sofia",
                    CountryId = 1
                },
                new City()
                {
                    Id = 2,
                    Name = "Varna",
                    CountryId = 1
            }  };

            var mockAddresses = new List<Address>()
            {
                new Address()
                {
                    Id = 1,
                    Street = "Street",
                    CityId = 1,
                    CountryId = 1
                },

                new Address()
                {
                    Id = 2,
                    Street = "Street1",
                    CityId = 1,
                    CountryId = 1
                }
            };

            var mockWarehouses = new List<Warehouse>()
            {
                new Warehouse
                {
                     Id = 1,
                     AddressId = 1
                },
                 new Warehouse
                {
                     Id = 2,
                     AddressId = 2
                }
            };
            var mockCountriesDbSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesDbSet = mockCities.AsQueryable().BuildMockDbSet();
            var mockAddressesDbSet = mockAddresses.AsQueryable().BuildMockDbSet();
            var mockWarehousesDbSet = mockWarehouses.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Countries).Returns(mockCountriesDbSet.Object);
            mockContext.Setup(db => db.Cities).Returns(mockCitiesDbSet.Object);
            mockContext.Setup(db => db.Addresses).Returns(mockAddressesDbSet.Object);
            mockContext.Setup(db => db.Warehouses).Returns(mockWarehousesDbSet.Object);

            mockContext.Setup(db => db.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();

            var service = new WarehouseService(mockContext.Object);


            var result = await service.TotalWarehousesAsync();

            Assert.AreEqual(result.Warehouses, 2);
        }
    }
}
